def measure(num=1)
  times = []
  num.times do
    time01 = Time.now
    yield
    time02 = Time.now
    times.push(time02-time01)
  end
  sum = 0
  times.each {|i| sum += i}
  avg_elapsed_time = sum / num
  return avg_elapsed_time
end

def reverser
  string = yield
  array = string.split(' ')
  array.each {|w| w.reverse!}
  new_string = array.join(' ')
  return new_string
end

def adder(num=1)
  yield + num
end

def repeater(num=1)
  num.times do yield; end
end
